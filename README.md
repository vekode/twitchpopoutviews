# twitchpopoutviews

## Intro
This is just a notifier for twitch chat popout. It currently will ding you everytime your chat recieves a new message. It only works on chat popout, so you must popout your chat.

## Prerequisites
1. This chrome extesion - https://chrome.google.com/webstore/detail/user-javascript-and-css/nbhcbdghjpllgmfilhnhkllmkecfmpld
1. Twitch App ID - you will have to register an app with twitch. Contact shawdae#9120 on discord if you need help obtaining this. I can help unless this gets used a lot more, in which case i'll stop being lazy and write a better readme.

## Configuration
1. Use https://www.twitch.tv/popout/ as the url in the chrome extension settings. 
2. Make sure all options in the chrome extension are enable, except the "Highest CSS Priority".
3. Paste the JS on the left and the CSS on the right.
4. Put in your app id in the JS at the top.


## Notes
1. This is very crude and no there is no warranty. GANG GANG